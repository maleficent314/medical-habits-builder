package com.mhb.medicalhabitsbuilder.dao.entities.usedstats;

public enum FrequencyMode {
    YEARLY,MONTHLY,WEEKLY,DAILY,HOURLY
}