package com.mhb.medicalhabitsbuilder.dao;

import com.mhb.medicalhabitsbuilder.dao.entities.Patient;
import com.mhb.medicalhabitsbuilder.security.PatientCoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public final class PatientBuilder {

    public Patient createPatient(PatientCoder patientCoder, PasswordEncoder passwordEncoder) {
        Patient patient = new Patient(
                patientCoder.getUsername(),
                passwordEncoder.encode(patientCoder.getPassword()),
                patientCoder.getEmail(),
                patientCoder.getPhone_number(),
                patientCoder.getRole(),
                patientCoder.getFirst_name(),
                patientCoder.getLast_name(),
                patientCoder.getBirthday(),
                patientCoder.getGender(),
                patientCoder.getHeight(),
                patientCoder.getWeight()
        );
        return patient;
    }

}
