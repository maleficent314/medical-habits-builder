package com.mhb.medicalhabitsbuilder.dao.entities;

import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.FrequencyMode;
import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.MessagingMode;

public class HabitView {
    private Long id;
    private String topic;
    private String content;
    private MessagingMode messagingMode;
    private FrequencyMode frequencyMode;
    private Integer minute;
    private Integer hour;
    private Integer day;
    private Integer month;
    private Integer dayOfWeek;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MessagingMode getMessagingMode() {
        return messagingMode;
    }

    public void setMessagingMode(MessagingMode messagingMode) {
        this.messagingMode = messagingMode;
    }

    public FrequencyMode getFrequencyMode() {
        return frequencyMode;
    }

    public void setFrequencyMode(FrequencyMode frequencyMode) {
        this.frequencyMode = frequencyMode;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public HabitView() {
    }

    public HabitView(Habit habit) {
        this.id = habit.getId();
        this.topic = habit.getTopic();
        this.content = habit.getContent();
        this.messagingMode = habit.getMessagingMode();
        this.frequencyMode = habit.getFrequencyMode();
        this.minute = habit.getMinute();
        this.hour = habit.getHour();
        this.day = habit.getDay();
        this.month = habit.getMonth();
        this.dayOfWeek = habit.getDayOfWeek();
    }
}
