package com.mhb.medicalhabitsbuilder.dao;

import com.mhb.medicalhabitsbuilder.dao.entities.Habit;
import com.mhb.medicalhabitsbuilder.dao.entities.Patient;
import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.FrequencyMode;
import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.Gender;
import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.MessagingMode;
import com.mhb.medicalhabitsbuilder.security.PatientCoder;
import com.mhb.medicalhabitsbuilder.service.HabitService;
import com.mhb.medicalhabitsbuilder.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Component
public class DoMockHabit {

    private HabitService habitService;
    private PatientService patientService;

    @Autowired
    public DoMockHabit(HabitService habitService, PatientService patientService) {
        this.habitService = habitService;
        this.patientService = patientService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillHabits(){
        List<Habit> habits = habitService.getAll();
        List<Patient> patients = patientService.getAll();
        int size =0;
        if (habits instanceof Collection) {
            size = ((Collection<Patient>) patients).size();
        }
        if (!(size>0)){
            patientService.addPatient(new PatientCoder("Agata57", "haslo", "medicalhabitsbuilder@gmail.com", "111222333","USER_ROLE", "Agata", "Kolanko", LocalDate.of(1900,10,10), Gender.FEMALE, 165.0, 50.5));
            patientService.addPatient(new PatientCoder("Popo", "haslo", "medicalhabitsbuilder@gmail.com", "333666999", "USER_ADMIN", "Paweł", "Palec", LocalDate.of(200,10,10), Gender.MALE, 180.0, 78.6));
        }
        patients = patientService.getAll();
        Patient patient = patients.get(0);
        Patient patient1 = patients.get(1);

        if (habits instanceof Collection) {
            size = ((Collection<Habit>) habits).size();
        }
        if (!(size>0)){
            habitService.addHabit(new Habit("Powiadomienie", "Nie jedz tyle", MessagingMode.MAIL, FrequencyMode.DAILY, 15, 16, null, null, null, patient));
            habitService.addHabit(new Habit("Powiadomienie", "Nie jedz tyle", MessagingMode.MAIL, FrequencyMode.WEEKLY, 15, 16, null, null, 3, patient));
            habitService.addHabit(new Habit("Powiadomienie", "Nie jedz tyle", MessagingMode.MAIL, FrequencyMode.MONTHLY, 15, 16, 5, null, null, patient1));
            habitService.addHabit(new Habit("powiadomienie15", "Nie jedz tyle", MessagingMode.MAIL, FrequencyMode.HOURLY, 15, 16, 5, null, null, patient1));
            habitService.addHabit(new Habit("powiadomienie30", "Nie jedz tyle", MessagingMode.MAIL, FrequencyMode.HOURLY, 30, 16, 5, null, null, patient1));
            habitService.addHabit(new Habit("powiadomienie40", "Nie jedz tyle", MessagingMode.MAIL, FrequencyMode.HOURLY, 40, 16, 5, null, null, patient1));
            habitService.addHabit(new Habit("powiadomienie5", "Nie jedz tyle", MessagingMode.MAIL, FrequencyMode.HOURLY, 5, 16, 5, null, null, patient1));
        }

    }
}
