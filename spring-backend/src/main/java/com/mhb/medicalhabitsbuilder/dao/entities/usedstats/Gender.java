package com.mhb.medicalhabitsbuilder.dao.entities.usedstats;

public enum Gender {
    MALE, FEMALE, OTHER
}
