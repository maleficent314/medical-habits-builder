package com.mhb.medicalhabitsbuilder.dao.entities;

import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.FrequencyMode;
import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.MessagingMode;

import javax.persistence.*;

@Entity
public class Habit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String topic;
    private String content;
    private MessagingMode messagingMode;
    private FrequencyMode frequencyMode;
    private Integer minute;
    private Integer hour;
    private Integer day;
    private Integer month;
    private Integer dayOfWeek;

    @ManyToOne
    @JoinColumn(name="patient_id", referencedColumnName = "id")
    private Patient patient;

    public Habit() {
    }

    public Habit(String topic, String content, MessagingMode messagingMode, FrequencyMode frequencyMode, Integer minute, Integer hour, Integer day, Integer month, Integer dayOfWeek, Patient patient) {
        this.topic = topic;
        this.content = content;
        this.messagingMode = messagingMode;
        this.frequencyMode = frequencyMode;
        this.minute = minute;
        this.hour = hour;
        this.day = day;
        this.month = month;
        this.dayOfWeek = dayOfWeek;
        this.patient = patient;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MessagingMode getMessagingMode() {
        return messagingMode;
    }

    public void setMessagingMode(MessagingMode messagingMode) {
        this.messagingMode = messagingMode;
    }

    public FrequencyMode getFrequencyMode() {
        return frequencyMode;
    }

    public void setFrequencyMode(FrequencyMode frequencyMode) {
        this.frequencyMode = frequencyMode;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getCronExpression(){
        String ans = "0 ";
        ans = ans.concat(String.valueOf(minute));
        String hours = String.valueOf(hour);
        String days = String.valueOf(day);
        String months = String.valueOf(month);
        String daysOfWeek = null;

        switch (frequencyMode) {
            case HOURLY:
                hours="*";
            case DAILY:
            case WEEKLY:
                days = "*";
            case MONTHLY:
                months = "*";
            case YEARLY:
                daysOfWeek = "*";
        }

        if (frequencyMode.equals(frequencyMode.WEEKLY)){
            daysOfWeek = String.valueOf(dayOfWeek);
        }

        ans = ans.concat(" "+hours+" "+days+" "+months+" "+" "+" ?"+daysOfWeek);
        System.out.println(ans);
        return ans;
    }


    @Override
    public String toString() {
        return "Habit{" +
                "id=" + id +
                ", topic='" + topic + '\'' +
                ", content='" + content + '\'' +
                ", messagingMode=" + messagingMode +
                ", frequencyMode=" + frequencyMode +
                ", minute=" + minute +
                ", hour=" + hour +
                ", day=" + day +
                ", month=" + month +
                ", dayOfWeek=" + dayOfWeek +
                ", patient=" + patient +
                '}';
    }
}
