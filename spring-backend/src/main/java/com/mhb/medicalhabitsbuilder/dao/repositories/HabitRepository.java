package com.mhb.medicalhabitsbuilder.dao.repositories;

import com.mhb.medicalhabitsbuilder.dao.entities.Habit;
import com.mhb.medicalhabitsbuilder.dao.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HabitRepository extends JpaRepository<Habit,Long> {
    Iterable<Habit> findByPatient(Optional<Patient> patient);
}
