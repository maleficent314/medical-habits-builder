package com.mhb.medicalhabitsbuilder.dao.entities;

import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.Gender;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;
    private String password;
    private String email;
    private String phone_number;
    private String role;
    private String first_name;
    private String last_name;
    private LocalDate birthday;
    private Gender gender;
    private Double height;
    private Double weight;

    @OneToMany(targetEntity = Habit.class, mappedBy = "patient",cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Habit> habitList = new ArrayList<>();

    public Patient() {
    }

    public Patient(String username, String password, String email, String phone_number, String role, String first_name, String last_name, LocalDate birthday) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone_number = phone_number;
        this.role = role;
        this.first_name = first_name;
        this.last_name = last_name;
        this.birthday = birthday;
    }

    public Patient(String username, String password, String email, String phone_number, String role, String first_name, String last_name, LocalDate birthday, Gender gender) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone_number = phone_number;
        this.role = role;
        this.first_name = first_name;
        this.last_name = last_name;
        this.birthday = birthday;
        this.gender = gender;
    }

    public Patient(String username, String password, String email, String phone_number, String role, String first_name, String last_name, LocalDate birthday, Double height, Double weight) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone_number = phone_number;
        this.role = role;
        this.first_name = first_name;
        this.last_name = last_name;
        this.birthday = birthday;
        this.height = height;
        this.weight = weight;
    }

    public Patient(String username, String password, String email, String phone_number, String role, String first_name, String last_name, LocalDate birthday, Gender gender, Double height, Double weight) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone_number = phone_number;
        this.role = role;
        this.first_name = first_name;
        this.last_name = last_name;
        this.birthday = birthday;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
    }

    public Patient(Long id, String username, String password, String email, String phone_number, String first_name, String last_name, LocalDate birthday, Gender gender, Double height, Double weight) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone_number = phone_number;
        this.first_name = first_name;
        this.last_name = last_name;
        this.birthday = birthday;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public List<Habit> getHabitList() {
        return habitList;
    }

    public void setHabitList(List<Habit> habitList) {
        this.habitList = habitList;
    }
}
