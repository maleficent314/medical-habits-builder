package com.mhb.medicalhabitsbuilder.dao.entities.usedstats;

public enum MessagingMode {
    SMS, MAIL
}
