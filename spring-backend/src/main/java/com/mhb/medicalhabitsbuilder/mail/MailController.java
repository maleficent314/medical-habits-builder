package com.mhb.medicalhabitsbuilder.mail;

import com.mhb.medicalhabitsbuilder.mail.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
public class MailController {
    private MailService mailService;
    @Autowired
    public MailController(MailService mailService) {
        this.mailService = mailService;
    }
    @GetMapping("/sendMail")
    public String sendMail() throws MessagingException{
        mailService.sendMail(
                "medicalhabitsbuilder@gmail.com",
                "VERY IMPORTANT MESSAGE",
                "<b>we are cool, guys</b><br>;)",
                true);
        return "email has been sent";
    }
}
