package com.mhb.medicalhabitsbuilder.dump.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDtoRepository extends JpaRepository<UserDto,Long> {

    Optional<UserDto> findByName(String name);

}
