package com.mhb.medicalhabitsbuilder.dump.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDtoService {
    private final UserDtoRepository userDtoRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserDtoService(UserDtoRepository userDtoRepository) {
        this.userDtoRepository = userDtoRepository;
    }

    public void addUser(User user){
        UserDtoBuilder userDtoBuilder = new UserDtoBuilder();
        userDtoRepository.save(userDtoBuilder.createUserDto(user, passwordEncoder));
    }
    public Optional<UserDto> getUser(String name) {
        return userDtoRepository.findByName(name);
    }
//    @Override
//    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
//        UserDetails user = org.springframework.security.core.userdetails.User.withUsername(name)
//                .build();
//        Optional<UserDto> userDto = userDtoRepository.findByName(name);
//
//        userDto.orElseThrow(()-> new UsernameNotFoundException("User not found: " + name));
//
//        return user;
//    }
}