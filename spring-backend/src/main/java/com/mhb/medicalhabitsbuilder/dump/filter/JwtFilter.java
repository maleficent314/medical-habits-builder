//package com.mhb.medicalhabitsbuilder.dump.filter;
//
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.Jws;
//import io.jsonwebtoken.Jwts;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Collections;
//import java.util.Set;
//
//public class JwtFilter extends BasicAuthenticationFilter {
//    public JwtFilter(AuthenticationManager authenticationManager) {
//        super(authenticationManager);
//    }
//
//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
//        String header = request.getHeader("Authorization");
////        System.out.println(header);
//        UsernamePasswordAuthenticationToken authenticationResults = getAuthenticationByToken(header);
//        SecurityContextHolder.getContext().setAuthentication(authenticationResults);
//        chain.doFilter(request,response);
//    }
//    private UsernamePasswordAuthenticationToken getAuthenticationByToken(String header){
//        //hard coded signing key.
////        System.out.println(header);
//        Jws<Claims> claimsJws = Jwts.parser().setSigningKey("admin")
//                .parseClaimsJws(header.replace("Bearer ",""));
////        System.out.println(claimsJws.getBody().get("sub").toString());
//        String username = claimsJws.getBody().get("sub").toString();
//        String role = claimsJws.getBody().get("role").toString();
//        //rola
//        Set<SimpleGrantedAuthority> simpleGrantedAuthorities = Collections.singleton(new SimpleGrantedAuthority(role));
//        return new UsernamePasswordAuthenticationToken(username,null,simpleGrantedAuthorities);
//    }
//}
