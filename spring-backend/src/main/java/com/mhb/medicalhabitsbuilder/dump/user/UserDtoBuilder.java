package com.mhb.medicalhabitsbuilder.dump.user;

import org.springframework.security.crypto.password.PasswordEncoder;

public class UserDtoBuilder {

    public UserDto createUserDto(User user, PasswordEncoder passwordEncoder){
        UserDto userDto = new UserDto(
                user.getName(),
                passwordEncoder.encode(user.getPassword()),
                user.getRole());
        return userDto;
    }
}
