package com.mhb.medicalhabitsbuilder.security.configuration;

import com.mhb.medicalhabitsbuilder.security.PasswordEncoderConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
//    private DataSource dataSource;
//    private PasswordEncoderConfiguration passwordEncoder;
//
//    @Autowired
//    public WebSecurityConfiguration(DataSource dataSource, PasswordEncoderConfiguration passwordEncoder) {
//        this.dataSource = dataSource;
//        this.passwordEncoder = passwordEncoder;
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.jdbcAuthentication()
//                .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM user WHERE u.name=?")
//                .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM user u WHERE u.name = ?")
//                .dataSource(dataSource)
//                .passwordEncoder(passwordEncoder.passwordEncoder());
//    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("habit/**")
                .antMatchers("/api/token","/console/**")
                .antMatchers("/api/product","/api/product/all")
                .antMatchers("/api/order","/api/order/all")
                .antMatchers("/**")
                .antMatchers(HttpMethod.POST,"/api/order");
    }
//                .antMatchers(HttpMethod.GET,"/api/product/*")
//                .antMatchers(HttpMethod.GET,"/api/product").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/product/all").permitAll())
//                .antMatchers(HttpMethod.GET,"/api/order").permitAll()
//                .antMatchers(HttpMethod.GET,"/api/order/all").permitAll()
//
//                .antMatchers(HttpMethod.POST,"/api/order").permitAll()
//        web.ignoring("/api/token");
//        jdbc:h2:file:./shopdatabase
//        /console/**

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/api/customer","/api/customer/all").hasAnyRole("CUSTOMER","ADMIN")
                .antMatchers("/api/admin/*").hasRole("ADMIN")
                .and()
                .csrf().disable(); //allows to access the token with POSTMAN, decreases security though
//                .addFilter(new JwtFilter(authenticationManager()));
    }
}
