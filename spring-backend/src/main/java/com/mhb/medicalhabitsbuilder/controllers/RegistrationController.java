package com.mhb.medicalhabitsbuilder.controllers;

import com.mhb.medicalhabitsbuilder.dao.PatientBuilder;
import com.mhb.medicalhabitsbuilder.dao.entities.Patient;
import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.Gender;
import com.mhb.medicalhabitsbuilder.security.Credentials;
import com.mhb.medicalhabitsbuilder.security.NewPatient;
import com.mhb.medicalhabitsbuilder.security.PatientCoder;
import com.mhb.medicalhabitsbuilder.service.HabitService;
import com.mhb.medicalhabitsbuilder.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;

@Controller
public class RegistrationController {
    private PatientService patientService;
    @Autowired
    public RegistrationController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/reg")
    public String getReg(Model model) {
        model.addAttribute("newPatient",new NewPatient());
        return "registration";
    }
    @PostMapping("/checking")
    public String checking(@RequestBody @ModelAttribute NewPatient newPatient) {
        // checking if the user exists
        PatientCoder patientCoder = new PatientCoder(newPatient.getUsername(),newPatient.getPassword(),
                newPatient.getEmail(), newPatient.getPhoneNumber(),"USER_ROLE",newPatient.getFirstName(),
                newPatient.getLastName(),LocalDate.now(),Gender.OTHER,70.0,180.0);
        patientService.addPatient(patientCoder);
        return "index";
    }

}
