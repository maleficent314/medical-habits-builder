package com.mhb.medicalhabitsbuilder.controllers;

import com.mhb.medicalhabitsbuilder.dao.entities.Habit;
import com.mhb.medicalhabitsbuilder.dao.entities.HabitView;
import com.mhb.medicalhabitsbuilder.dao.entities.Patient;
import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.FrequencyMode;
import com.mhb.medicalhabitsbuilder.dao.entities.usedstats.MessagingMode;
import com.mhb.medicalhabitsbuilder.security.Credentials;
import com.mhb.medicalhabitsbuilder.security.PasswordEncoderConfiguration;
import com.mhb.medicalhabitsbuilder.service.HabitService;
import com.mhb.medicalhabitsbuilder.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class PatientController {

    private PatientService patientService;
    private HabitService habitService;
    private Credentials credentialsSaved;
    private Patient patientSaved;
    private Habit habitSaved;
    private PasswordEncoderConfiguration passwordEncoderConfiguration;

    @Autowired
    public PatientController(PatientService patientService, HabitService habitService,
                             PasswordEncoderConfiguration passwordEncoderConfiguration) {
        this.patientService = patientService;
        this.habitService = habitService;
        this.passwordEncoderConfiguration = passwordEncoderConfiguration;
    }

    @GetMapping("/signIn")
    public String sign(Model model) {
        model.addAttribute("newCredentials", new Credentials());
        return "login";
    }

    @GetMapping("/profile")
    public String goToProfile(@ModelAttribute Credentials credentials, Model model) {
        String username = credentials.getUsername();
        String password = credentials.getPassword();
        System.out.println(username + " , " + password);
        try{
            Patient patient = patientService
                    .getPatient(username).get();

            String passwordStored = patient.getPassword();

            boolean doesMatch = passwordEncoderConfiguration.passwordEncoder().matches(password,passwordStored);

            System.out.println(doesMatch);

            if(doesMatch){

                String email = patient.getEmail();

                List<Patient> patientsAll = patientService.getAll();

                for (Patient p : patientsAll) {

                    if (email.equals(p.getEmail())) {
                        model.addAttribute("idPatient", p.getId());
                        model.addAttribute("userName", p.getUsername());
                        model.addAttribute("phoneNumber", p.getPhone_number());
                        model.addAttribute("firstName", p.getFirst_name());
                        model.addAttribute("lastName", p.getLast_name());
                        model.addAttribute("birthday", p.getBirthday());
                        model.addAttribute("gender", p.getGender());
                        model.addAttribute("height", p.getHeight());
                        model.addAttribute("weight", p.getWeight());
                        model.addAttribute("email", p.getEmail());
                        System.out.println("Sign in succeed");

                        List<HabitView> habitsAll = (List<HabitView>) habitService.getHabitsByUsername(p.getUsername());
                        model.addAttribute("habitList", habitsAll);

                        patientSaved = new Patient(p.getId(), p.getUsername(), p.getPassword(),
                                p.getEmail(), p.getPhone_number(), p.getFirst_name(), p.getLast_name(), p.getBirthday(),
                                p.getGender(), p.getHeight(), p.getWeight());
                        return "profile_view";
                    }
                }

            } else{
                System.out.println("Wrong password or login given.");
                return "redirect:/signIn";
            }

        }catch (Exception e){
            System.out.println("Error 1.");
            return "redirect:/signIn";
        }
        System.out.println("Error 2.");
        return "redirect:/signIn";
    }

    @RequestMapping("/deleteHabit/{id}")
    public String cancelHabit(@PathVariable Long id) {
        habitService.deleteHabit(id);
        return "redirect:/trialBack";
    }

    @GetMapping("/formStart")
    public String startMakeAppointment() {
        return "form1";
    }

    @RequestMapping("/addHabit")
    public String makeAppointment(@RequestParam(value = "content") String content,
                                  @RequestParam(value = "messagingMode") MessagingMode messagingMode,
                                  @RequestParam(value = "frequencyMode") FrequencyMode frequencyMode,
                                  Model model) {

        habitSaved = new Habit("Habit", content, messagingMode, frequencyMode,
                null, null, null, null, null, patientSaved);

        List minuteList = new ArrayList();
        List hourList = new ArrayList();
        List dayList = new ArrayList();
        List dayOfMonthList = new ArrayList();
        List monthList = new ArrayList();
        switch (habitSaved.getFrequencyMode()) {
            case HOURLY:
                for (int i = 1; i <= 60; i++) {
                    minuteList.add(i);
                }
                model.addAttribute("minuteList", minuteList);
                return "formHourly";

            case DAILY:
                for (int i = 0; i <= 59; i++) {
                    minuteList.add(i);
                }
                for (int i = 0; i <= 23; i++) {
                    hourList.add(i);
                }
                model.addAttribute("minuteList", minuteList);
                model.addAttribute("hourList", hourList);

                return "formDaily";

            case WEEKLY:
                for (int i = 0; i <= 59; i++) {
                    minuteList.add(i);
                }
                for (int i = 0; i <= 23; i++) {
                    hourList.add(i);
                }
                dayList.add("MONDAY");
                dayList.add("TUESDAY");
                dayList.add("WEDNESDAY");
                dayList.add("THURSDAY");
                dayList.add("FRIDAY");
                dayList.add("SATURDAY");
                dayList.add("SUNDAY");
                model.addAttribute("minuteList", minuteList);
                model.addAttribute("hourList", hourList);
                model.addAttribute("dayList", dayList);
                return "formWeekly";

            case MONTHLY:
                for (int i = 0; i <= 59; i++) {
                    minuteList.add(i);
                }
                for (int i = 0; i <= 23; i++) {
                    hourList.add(i);
                }
                for (int i = 1; i <= 31; i++) {
                    dayOfMonthList.add(i);
                }
                model.addAttribute("minuteList", minuteList);
                model.addAttribute("hourList", hourList);
                model.addAttribute("dayOfMonthList", dayOfMonthList);
                return "formMonthly";

            case YEARLY:
                for (int i = 0; i <= 59; i++) {
                    minuteList.add(i);
                }
                for (int i = 0; i <= 23; i++) {
                    hourList.add(i);
                }
                for (int i = 1; i <= 31; i++) {
                    dayOfMonthList.add(i);
                }
                for (int i = 1; i <= 12; i++) {
                    monthList.add(i);
                }
                model.addAttribute("minuteList", minuteList);
                model.addAttribute("hourList", hourList);
                model.addAttribute("dayOfMonthList", dayOfMonthList);
                model.addAttribute("monthList", monthList);
                return "formYearly";
        }
        return "profile_view";
    }

    @RequestMapping("/addHabitHourly")
    public String addHabitHourly(@RequestParam(value = "minuteChosen") int minute, Model model) {
        habitSaved.setMinute(minute);
        System.out.println(habitSaved);
        habitService.addHabit(habitSaved);
        return "redirect:/trialBack";
    }

    @RequestMapping("/addHabitDaily")
    public String addHabitDaily(@RequestParam(value = "minuteChosen") int minute,
                                @RequestParam(value = "hourChosen") int hour) {
        habitSaved.setMinute(minute);
        habitSaved.setHour(hour);
        System.out.println(habitSaved);
        habitService.addHabit(habitSaved);
        return "redirect:/trialBack";
    }

    @RequestMapping("/addHabitWeekly")
    public String addHabitWeekly(@RequestParam(value = "minuteChosen") int minute,
                                 @RequestParam(value = "hourChosen") int hour,
                                 @RequestParam(value = "dayChosen") String day) {
        habitSaved.setMinute(minute);
        habitSaved.setHour(hour);
        switch (day) {
            case "MONDAY":
                habitSaved.setDayOfWeek(1);
                break;
            case "TUESDAY":
                habitSaved.setDayOfWeek(2);
                break;
            case "WEDNESDAY":
                habitSaved.setDayOfWeek(3);
                break;
            case "THURSDAY":
                habitSaved.setDayOfWeek(4);
                break;
            case "FRIDAY":
                habitSaved.setDayOfWeek(5);
                break;
            case "SATURDAY":
                habitSaved.setDayOfWeek(6);
                break;
            case "SUNDAY":
                habitSaved.setDayOfWeek(7);
                break;
        }
        System.out.println(habitSaved);
        habitService.addHabit(habitSaved);
        return "redirect:/trialBack";
    }

    @RequestMapping("/addHabitMonthly")
    public String addHabitMonthly(@RequestParam(value = "minuteChosen") int minute,
                                  @RequestParam(value = "hourChosen") int hour,
                                  @RequestParam(value = "dayOfMonthChosen") int day) {
        habitSaved.setMinute(minute);
        habitSaved.setHour(hour);
        habitSaved.setDay(day);
        System.out.println(habitSaved);
        habitService.addHabit(habitSaved);
        return "redirect:/trialBack";
    }

    @RequestMapping("/addHabitYearly")
    public String addHabitYearly(@RequestParam(value = "minuteChosen") int minute,
                                 @RequestParam(value = "hourChosen") int hour,
                                 @RequestParam(value = "dayOfMonthChosen") int day,
                                 @RequestParam(value = "monthChosen") int month) {
        habitSaved.setMinute(minute);
        habitSaved.setHour(hour);
        habitSaved.setDay(day);
        habitSaved.setMonth(month);
        System.out.println(habitSaved);
        habitService.addHabit(habitSaved);
        return "redirect:/trialBack";
    }

    @GetMapping("/trialBack")
    public String backToProfile(Model model) {
        model.addAttribute("idPatient", patientSaved.getId());
        model.addAttribute("userName", patientSaved.getUsername());
        model.addAttribute("phoneNumber", patientSaved.getPhone_number());
        model.addAttribute("firstName", patientSaved.getFirst_name());
        model.addAttribute("lastName", patientSaved.getLast_name());
        model.addAttribute("birthday", patientSaved.getBirthday());
        model.addAttribute("gender", patientSaved.getGender());
        model.addAttribute("height", patientSaved.getHeight());
        model.addAttribute("weight", patientSaved.getWeight());
        model.addAttribute("email", patientSaved.getEmail());
        System.out.println("Sign in succeed");

        List<HabitView> habitsAll = (List<HabitView>) habitService.getHabitsByUsername(patientSaved.getUsername());
        model.addAttribute("habitList", habitsAll);
        return "profile_view";
    }
}
