package com.mhb.medicalhabitsbuilder.controllers;

import com.mhb.medicalhabitsbuilder.dao.entities.Habit;
import com.mhb.medicalhabitsbuilder.dao.entities.HabitView;
import com.mhb.medicalhabitsbuilder.dao.entities.Patient;
import com.mhb.medicalhabitsbuilder.service.HabitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/habit")
//needed for react js
@CrossOrigin
public class HabitController {
    private HabitService habitService;
    @Autowired
    public HabitController(HabitService habitService) {
        this.habitService = habitService;
    }
    @GetMapping
    public Iterable<HabitView> getByUsername(@RequestParam String username){
        return habitService.getHabitsByUsername(username);
    }
    @GetMapping("/all")
    public List<Habit> getAll(){
        return habitService.getAll();
    }

    @PostMapping("/add")
    public void addHabit(@RequestBody Habit habit){
        habitService.addHabit(habit);
    }
    @DeleteMapping("/delete")
    public void deleteHabit(@RequestParam Long id){
        habitService.deleteHabit(id);
    }
}
