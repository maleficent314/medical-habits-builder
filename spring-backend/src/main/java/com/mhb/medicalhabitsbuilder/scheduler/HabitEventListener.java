package com.mhb.medicalhabitsbuilder.scheduler;

import com.mhb.medicalhabitsbuilder.dao.entities.Habit;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.quartz.SchedulerException;
import org.springframework.stereotype.Component;

@Component
public class HabitEventListener  implements PostDeleteEventListener, PostInsertEventListener, PostUpdateEventListener {

    @Override
    public void onPostDelete(PostDeleteEvent postDeleteEvent) {
        final Object entity = postDeleteEvent.getEntity();
        if(entity instanceof Habit) {
            Habit habit = (Habit) entity;
            System.out.println(habit.getMinute());
            try {
                SchedulingClass.delecteSchedule(habit);
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPostInsert(PostInsertEvent postInsertEvent) {
        final Object entity = postInsertEvent.getEntity();

        if(entity instanceof Habit) {
            Habit habit = (Habit) entity;
            System.out.println(habit.toString());
            try {
                SchedulingClass.addCronSchedue(habit);
                System.out.println(habit.toString());
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPostUpdate(PostUpdateEvent postUpdateEvent) {
        final Object entity = postUpdateEvent.getEntity();
        if(entity instanceof Habit) {
            Habit habit = (Habit) entity;
            try {
                SchedulingClass.updateSchedule(habit);
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }


}
