package com.mhb.medicalhabitsbuilder.scheduler;

import com.mhb.medicalhabitsbuilder.dao.entities.Habit;
import com.mhb.medicalhabitsbuilder.mail.service.MailService;
import com.mhb.medicalhabitsbuilder.service.HabitService;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.List;

@Component
public class SchedulingClass {

    private static HabitService habitService;
    private MailService mailService;
    private static Scheduler scheduler;

    @Autowired
    public SchedulingClass(HabitService habitService,MailService mailService) throws ParseException, SchedulerException {
        this.habitService = habitService;
        this.mailService = mailService;
        scheduler = new StdSchedulerFactory().getScheduler();
        scheduler.start();
    }

//    @EventListener(ApplicationReadyEvent.class)
//    @SuppressWarnings({"rawtypes", "unchecked"})
//    public static void init() throws ParseException, SchedulerException {
//        cleanup();
//        List<Habit> habits = habitService.getAll();
//        String mail;
//        String topic;
//        String content;
//        for (Habit habit : habits) {
//            mail = habit.getPatient().getEmail();
//            System.out.println(mail + "przed");
//            topic = habit.getTopic();
//            System.out.println(topic + "przed");
//            content = habit.getContent();
//            System.out.println(content + "przed");
//            JobDetail jobDetail = JobBuilder.newJob().ofType(MailingJob.class).withIdentity("kokjambo"+habit.getId())
//                    .usingJobData("mail", mail)
//                    .usingJobData("topic", topic)
//                    .usingJobData("content", content)
//                    .build();
//
//            CronTrigger trigger1 = TriggerBuilder.newTrigger().withIdentity("dummyTriggerName" + habit.getId(),"|daw").withSchedule(CronScheduleBuilder.cronSchedule(habit.getCronExpression())).build();
//
//            scheduler.start();
//            scheduler.scheduleJob(jobDetail,trigger1);
//        }
//    }

    public static void addCronSchedue(Habit habit) throws SchedulerException {
        String mail = habit.getPatient().getEmail();
        System.out.println(mail + "przed");
        String topic = habit.getTopic();
        System.out.println(topic + "przed");
        String content = habit.getContent();
        System.out.println(content + "przed");
        JobDetail jobDetail = JobBuilder.newJob().ofType(MailingJob.class).withIdentity("kokjambo"+habit.getId())
                .usingJobData("mail", mail)
                .usingJobData("topic", topic)
                .usingJobData("content", content).storeDurably(false)
                .build();

        CronTrigger trigger1 = TriggerBuilder.newTrigger().withIdentity("dummyTriggerName" + habit.getId(),"|daw").withSchedule(CronScheduleBuilder.cronSchedule(habit.getCronExpression())).build();

        scheduler.start();
        scheduler.scheduleJob(jobDetail,trigger1);
    }

    public static void delecteSchedule(Habit habit) throws SchedulerException {
       scheduler.unscheduleJob(TriggerKey.triggerKey("dummyTriggerName" + habit.getId(),"|daw"));
    }

    public static void updateSchedule(Habit habit) throws SchedulerException {
        delecteSchedule(habit);
        addCronSchedue(habit);
    }

    private static void cleanup() {
        System.out.println("Cleaning up schedules in scheduler");
        try {
            scheduler.clear();
        } catch (final SchedulerException e) {
            System.out.println("Exception clearing scheduler: "+ e.toString());
        }
    }

}





