package com.mhb.medicalhabitsbuilder.scheduler;


import com.mhb.medicalhabitsbuilder.mail.service.MailService;
import org.quartz.*;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


import javax.mail.MessagingException;

@Service
public class MailingJob extends QuartzJobBean {

     private MailService mailService;
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public MailingJob() {
    }

    //    @Autowired
//    public MailingJob(MailService mailService) {
//        this.mailService = mailService;
//    }

    @Override
    public void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();

        String mail = jobDataMap.getString("mail");
        String topic = jobDataMap.getString("topic");
        String content = jobDataMap.getString("content");

        System.out.println(mail + "po");

        System.out.println(topic + "po");

        System.out.println(content + "po");
        try {
            MailService.sendMail(mail, topic, content, false);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        System.out.println("Hello Quartz!");

    }

}
