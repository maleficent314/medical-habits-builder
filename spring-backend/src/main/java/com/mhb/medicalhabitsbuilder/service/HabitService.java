package com.mhb.medicalhabitsbuilder.service;

import com.mhb.medicalhabitsbuilder.dao.entities.Habit;
import com.mhb.medicalhabitsbuilder.dao.entities.HabitView;
import com.mhb.medicalhabitsbuilder.dao.repositories.HabitRepository;
import com.mhb.medicalhabitsbuilder.dao.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class HabitService {

    private HabitRepository habitRepository;
    private PatientRepository patientRepository;

    @Autowired
    public HabitService(HabitRepository habitRepository, PatientRepository patientRepository) {
        this.habitRepository = habitRepository;
        this.patientRepository = patientRepository;
    }

    public List<Habit> getAll() {
        return habitRepository.findAll();
    }

    public Habit addHabit(Habit habit){
        return habitRepository.save(habit);
    }

    public Iterable<HabitView> getHabitsByUsername(String username) {
        try {
            Iterable<Habit> habitIterable = habitRepository.findByPatient(patientRepository.findByUsername(username));

            List<HabitView> habitViewList = new ArrayList<>();

            for(Habit h : habitIterable){
                habitViewList.add(new HabitView(h));
            }

            return habitViewList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void deleteHabit(Long id) {
        habitRepository.deleteById(id);
    }
}
