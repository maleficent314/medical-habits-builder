package com.mhb.medicalhabitsbuilder.service;

import com.mhb.medicalhabitsbuilder.dao.PatientBuilder;
import com.mhb.medicalhabitsbuilder.dao.entities.Habit;
import com.mhb.medicalhabitsbuilder.dao.entities.Patient;
import com.mhb.medicalhabitsbuilder.dao.repositories.PatientRepository;
import com.mhb.medicalhabitsbuilder.security.PatientCoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PatientService {

    private PatientRepository patientRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<Patient> getAll() {
        return patientRepository.findAll();
    }

    public Patient addPatient(PatientCoder patientCoder){
        PatientBuilder patientBuilder = new PatientBuilder();
        return patientRepository.save(patientBuilder.createPatient(patientCoder,passwordEncoder));
    }

    public Optional<Patient> getPatient(String name){return patientRepository.findByUsername(name);}
}
