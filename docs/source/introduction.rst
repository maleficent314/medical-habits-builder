Introduction
======================

Medical Habits Builder
----------------------
MHB is a free software which helps you to be more *systematic*, *motivated* and *updated*.

.. image:: mhb.png
