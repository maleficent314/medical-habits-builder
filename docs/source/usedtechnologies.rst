Used technologies
===========================

Back-end
---------------------------
We did use Spring since our "native" programming language is Java and we do feel the most comfortable in it. More over, this framework is well known within our team.

Spring Security
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Users' credentials are safely stored in the database. The passwords are encrypted.

Spring Data with PostgreSQL
^^^^^^^^^^^^^^^^^^^^^^^^^^^
A SQL database allows to maintain persistence of the data stored and managed by our application.

Spring MVC
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Model-View-Controller design pattern makes the application more transparent and clear.

Front-end
---------------------------

React-js
^^^^^^^^^^^^^^^^^^^^^^^^^^^
We had been trying to implement an React-js app but we were stuck due to our tiny experience with Javascript and React-js. Consequently, we decided to change the technology.

Thymeleaf
^^^^^^^^^^^^^^^^^^^^^^^^^^^
We changed React-js to Thymeleaf since a Multi Page Application (MPA) was a better match than Single Page Application to this project. Additionally, our knowledge, experience and skills of it were more advanced than previously mentioned technologies.

Bootstrap
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Bootstrap was used in this project because of its simplicity and beauty.

Mobile
---------------------------

Android
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Initially, it had been planned to add an android App but eventually we didn't manage to create it.

WearOS
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Initially, it had been planned to add an android App but eventually we didn't manage to create it.

