Diagrams of application 
=======================

Architecture
-------------------------

Client-server architecture
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. image:: client-server.png

Use cases
----------------------
.. uml::

    Client -> Application: registers, logs in, adds a new habit
    Application ---> Database: authenticates, adds the habit to the scheduler
    Database ---> Application: authenticates, gives information about the habit
    Application -> Client: sends mails according to the chosen frequency

System Context diagram
----------------------
.. image:: c41.png
