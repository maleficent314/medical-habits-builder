Guide
======================

Create an account
----------------------

In order to become a master of your habits create a free account. Your credentials are going to be safely stored.

Manage your habits
----------------------

Add your own habit
^^^^^^^^^^^^^^^^^^^^^^

Choose a name for the habit, messaging mode (SMS incoming!), frequency.

Delete your own habit
^^^^^^^^^^^^^^^^^^^^^^

You can delete your habit - hopefully, once you achieved your goal


