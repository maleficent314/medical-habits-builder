Welcome to Medical Habits Builder documentation.
================================================

You will find here an introduction to Medical Habits Builder (MHB), guide to the application, application architecture, used technologies, diagram of the application, important classes, et cetera.

.. toctree::
    :maxdepth: 3
    :caption: Table of contents

    introduction
    guide
    usedtechnologies
    appdiagrams
    classes


