Important and complicated classes
=================================

MailingJob
----------------------

This class extends QuartzJobBean. 
Creation of empty constructor is forced by the way QuartzJobs are constructed.
In the method executeInternal firstly datamap is taken. After that, based on the content of the map, an attempt to send mail based on it is made. The datamap is a set which is called in the class called: SchedulingClass.

SchedulingClass
----------------------

In the constructor it creates scheduler using StdSchedulerFactory.
There are three methods in this class.

addCronSchedule
^^^^^^^^^^^^^^^^^^^^^

it uses input Habit object to create scheduled Mailing Job based on it.

deleteSchedule
^^^^^^^^^^^^^^^^^^^^^

it uses input Habit object to delete scheduled task that was previously based on it.

updateSchedule
^^^^^^^^^^^^^^^^^^^^^

it uses input Habit object to firstly delete existing task previously based on it and then creates new scheduled task based on the new version of an Habit object.

HabitEventListener
---------------------- 

this class is used to handle events on database in particular changes in habit table.
It implements hibernate interfaces of: PostDeleteEventListener, PostInsertEventListener, PostUpdateEventListener.

onPostDelete 
^^^^^^^^^^^^^^^^^^^^^

this method is used to call function deleteSchedule from SchedulingClass whenever habit is deleted from the table. It is used to delete scheduled task based on it.

onPostInsert 
^^^^^^^^^^^^^^^^^^^^^

this method is used to call function addCronSchedue from SchedulingClass whenever habit is added to the table. It is used to create scheduled task based on it.

onPostUpdate 
^^^^^^^^^^^^^^^^^^^^^

this method is used to call function updateSchedule from SchedulingClass whenever habit is changed in the table. It is used to change scheduled task based on it.

HibernateListenerConfigurer
---------------------------

This class is used to register HabitEventListener class as eventListeners with the init method.


